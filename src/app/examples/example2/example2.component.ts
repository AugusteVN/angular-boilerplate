import { Observable } from 'rxjs/Observable';
import { ExampleService } from './../../providers/example.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector   : 'app-example2',
  templateUrl: './example2.component.html',
  styleUrls  : ['./example2.component.scss']
})
export class Example2Component implements OnInit {

  collection$: Observable<any>;
  filterBy   : string;

  constructor(private _exService: ExampleService) {}

  ngOnInit() {
    this.collection$ = this._exService.getAll();
  }

  filter(value: string): void {
    this.filterBy = value;
  }

  /* trackByFn in ngFor (OPTIONAL)
    * What? returns the unique identifier for each item.
    * Why? When an array changes, Angular re-renders the whole DOM but with 'trackBy',
    * Angular will know which element has changed and will only make DOM changes for that element.
  */

  trackByFn(index, item) {
    return item.id;
  }

}
