import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Example1Component } from './example1/example1.component';
import { Example2Component } from './example2/example2.component';
import { ComponentsModule } from '../components/components.module';
import { PipesModule } from '../pipes/pipes.module';

@NgModule({
  declarations: [Example1Component, Example2Component],
  imports     : [
    CommonModule,
    FormsModule,
    ComponentsModule,
    PipesModule
  ],
  exports: [Example1Component, Example2Component]
})
export class ExamplesModule { }
