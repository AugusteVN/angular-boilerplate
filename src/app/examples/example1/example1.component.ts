import { Component, OnInit } from '@angular/core';

@Component({
  selector   : 'app-example1',
  templateUrl: './example1.component.html',
  styleUrls  : ['./example1.component.scss']
})
export class Example1Component implements OnInit {

  title: String = `I'm a two-way bound property, change me!`;

  constructor() { }

  ngOnInit() {
  }

}
