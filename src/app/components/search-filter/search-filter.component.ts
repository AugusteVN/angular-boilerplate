import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector   : 'app-search-filter',
  templateUrl: './search-filter.component.html',
  styleUrls  : ['./search-filter.component.scss']
})
export class SearchFilterComponent implements OnInit {

  @Output() filterEmitted = new EventEmitter();  // Output event on component: '<app-search-filter (filterEmitted)=""></app-search-filter>'

  constructor() { }

  ngOnInit() {
  }

  emitFilter(e: any): void {
    const filter = e.target.value;
    this.filterEmitted.emit(filter);
  }
}
