import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SearchFilterComponent } from './search-filter/search-filter.component';

// For global components

@NgModule({
  declarations: [SearchFilterComponent],
  imports     : [
    CommonModule
  ],
  exports: [SearchFilterComponent]
})
export class ComponentsModule { }
