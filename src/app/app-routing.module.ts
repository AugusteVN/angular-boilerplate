import { ExamplesModule } from './examples/examples.module';
import { Example2Component } from './examples/example2/example2.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { Example1Component } from './examples/example1/example1.component';

const routes: Routes = [
  { path: 'example1', component: Example1Component },
  { path: 'example2', component: Example2Component },
  { path: 'example1',
    redirectTo: '',
    pathMatch : 'full'
  },
  { path: '**', component: Example1Component }
];

@NgModule({
  imports: [
    ExamplesModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
