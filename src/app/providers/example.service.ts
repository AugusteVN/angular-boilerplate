import { ContractModel } from './../models/contract.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs/observable';
import 'rxjs/add/operator/map';

@Injectable({
  providedIn: 'root'
})
export class ExampleService {

  // HTTP request examples using observables (rxjs)

  private url: string = ContractModel.EX_URL;

  constructor(private _http: HttpClient) {} // Make sure HttpClientModule is imported in root (../app.module.ts)

  getAll() {
    return this._http.get(this.url);
  }

  getByKey(key: string): Observable<any> {
    return this.getAll() // call all items and get item by ID using LINQ alike stuff
            .map((items: any[]) => items.find(p => p.key === key));
  }

  add(data: any): Observable<any> {
    const dataStringified = JSON.stringify(data);
    const headers         = new HttpHeaders();

    headers.append('Content-Type', 'application/json');

    return this._http
            .post(this.url, dataStringified, { headers })
            .map((response: Response) => <any>response.json());
  }

  update(data: any, id: string): Observable<any> {
    const dataStringified = JSON.stringify(data);
    const headers         = new HttpHeaders();

    headers.append('Content-Type', 'application/json');

    return this._http
            .put(this.url, dataStringified, { headers });
  }

  removeById(id: string): Observable<any> {

    const headers = new HttpHeaders();

    headers.append('Content-Type', 'application/json');

    return this._http
            .delete(this.url, { headers });
  }

}
