import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { OffCanvasComponent } from './off-canvas/off-canvas.component';
import { AppRoutingModule } from '../app-routing.module';

@NgModule({
  declarations: [HeaderComponent, MainComponent, FooterComponent, OffCanvasComponent],
  imports     : [
    CommonModule,
    AppRoutingModule
  ],
  exports: [HeaderComponent, MainComponent, FooterComponent, OffCanvasComponent]
})
export class TemplatesModule { }
