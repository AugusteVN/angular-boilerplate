import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchFilter'
})
export class SearchFilterPipe implements PipeTransform {

  transform(value: any, filterBy: string, args?: any): any {

    filterBy = filterBy ? filterBy.toLocaleLowerCase() : null;

    return filterBy ? value.filter((entry: any) =>
      entry.item.toLocaleLowerCase().indexOf(filterBy) !== -1) : value;
  }

}
